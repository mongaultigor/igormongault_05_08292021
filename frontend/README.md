# Orinoco #

This is the back end server and front end for Project 5 of the Junior Web Developer path.

### Prerequisites ###

You will need to have Node and `npm` installed locally on your machine.

### Installation ###

Clone this repo. From within the backend folder, run `npm install`. You 
can then run the server in backend folder with `npm run start`. 
The server should run on `localhost` with default port `3000`. If the
server runs on another port for any reason, this is printed to the
console when the server starts, e.g. `Listening on port 3001`.

##### Launch the browser ######
From within the backend folder, run `npm install`. You can then run the  webpack server `npm run dev`, it will open automatically the main page. The webpack server should run on `localhost` with default port `9000`