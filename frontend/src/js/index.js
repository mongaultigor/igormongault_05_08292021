import Requete from "./modele/requete.js";
import Heading from '../components/heading/heading';
import Footer from "../components/footer/footer.js";

import '../../node_modules/bootstrap/dist/css/bootstrap.min.css';
import '../components/heading/heading.scss';
import '../components/Pages/index.scss'


import '../../node_modules/jquery/dist/jquery.min';
import '../../node_modules/bootstrap/dist/js/bootstrap.bundle.min';


let mainContent = "";
const main = document.querySelector('main');
const requete = new Requete();
const url = "http://localhost:3000/api/teddies";

 try {
    requete.get(url).
    then( liste => {
       const head = new Heading();
       head.render(liste);
       
       mainContent  = `<div class="jumbotron text-center my-3">
       <div class="container">
         <h1 class="jumbotron-heading">Orinoco shop</h1>
         <p class="lead text-muted mb-0">
           Nous sommes un site de vente en ligne des ours en peluche, 
           à des prix abordables. Veuillez cliquer sur une
            peluche pour plus de détails.
         </p>
       </div>
     </div>`;
      
     mainContent += `<div class="container mt-2">
     <div class="row">
       <div class="col-sm">
         <div class="card">
           <div class="card-header bg-primary text-white text-uppercase">
             <h2 class="card-title">
               Nos produits
             </h2>
           </div>
           <div class="card-body">
             <div class="row">`;
       for (const article of liste) {
           mainContent += `<div class="col-sm col-md-6 col-lg-4 col-xl-3 my-2"><div class="card">
           <img src="${article.produit.imageUrl}" alt="card image cap" class="card-img-top img-thumbnail rounded-1">
           <div class="card-body">
             <h3 class="card-title">
              ${article.produit.name}
             </h3>
             <p class="card-text">${article.produit.description}</p>
             <div class="row">
               <div class="col-12 text-center">
                 <p class="btn btn-secondary btn-block w-50">${article.produit.price/100} $</p>
               </div>
               <div class="col-12">
                 <a href="produits.html?id=${article.produit._id}" class="btn btn-info btn-block card-link w-100">Détails du produit</a>
               </div>
               </div>
           </div>
         </div></div>`
       }
      
      mainContent += `</div></div></div></div></div></div>`;
      
      main.innerHTML = mainContent;

      const footer = new Footer();
      footer.render();
      
    });

 } catch (error) {
     console.log(error);
 }


