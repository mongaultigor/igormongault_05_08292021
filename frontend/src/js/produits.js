import Requete from "./modele/requete.js";
import choixCouleur from "../components/Pages/selectColors.js";
import * as cartManager from "./modele/cartManager";
import Heading from "../components/heading/heading";
import Footer from "../components/footer/footer.js";


import "../../node_modules/bootstrap/dist/css/bootstrap.min.css";
import "../components/heading/heading.scss";

import "../components/Pages/produits.scss";

import "jquery/dist/jquery.min";
import "bootstrap/dist/js/bootstrap.bundle.min";

let mainContent = "";
const main = document.querySelector("main");
const requete = new Requete();
const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);
let id = urlParams.get("id");
const url = "http://localhost:3000/api/teddies";
const urlById = "http://localhost:3000/api/teddies/" + id;

try {

  // affiche l'en tete de la page
  
  requete.get(url).then((liste) => {
    const head = new Heading();
    head.render(liste);
  });

  requete.getById(urlById).then((leProduit) => {
    if (leProduit != undefined) {
      mainContent = `<div class="jumbotron text-center my-3">
                            <div class="container">
                                <h1 class="jumbotron-heading">${leProduit.produit.name}</h1>
                                <p class="lead text-muted mb-0">
                                  Details du produit
                                </p>
                             </div>
                          </div>`;

      mainContent += `<div class="container">
            <div class="row">
              <!-- Image -->
              <div class="col-12 col-lg-6">
                <div class="card bg-light mb-3">
                  <div class="card-body">
                    <a href="#" class="img-fluid" data-toggle="modal" data-target="#produitModal" >
                     <img src="${
                       leProduit.produit.imageUrl
                     }" alt="Photo Ours peluche">
                    </a>
                  </div>
                </div>
              </div>
              <!-- Ajout au panier-->
              <div class="col-12 col-lg-6 add_cart_block">
                <div class="card bg-light mb-3">
                      <div class="card-body">
                          <p class="price my-3">${leProduit.produit.price/100} $</p>
                          <form >
                            <div class="form-group mb-4 w-50">
                              <label class="mb-2" for="colors">Couleur</label>
                              <select id="colors" class="custom-select">
                                 <option value="" selected="">Choisir la couleur</option>
                                  ${choixCouleur(leProduit.colors)}
                                 </select>
                                 <small id="errorColor"></small>
                            </div>
                            <div class="form-group mb-3">
                                     <label class="mb-2">Quantité :</label>
                                     <div class="input-group w-25">
                                         <input type="number" step="1" class="form-control" id="quantity" name="quantity" min="1" max="100" value="1">
                                     </div>
                                     <small id="errorQty"></small>
                             </div>
                             <a href="panier.html" class="btn btn-success btn-lg btn-block text-uppercase w-100 my-3" id="addInCart">
                             <i class="fa fa-shopping-cart"></i>
                             Ajouter au panier
                             </a>
                          </form>
                      </div>
                </div>      
              </div>
            </div>
            <!-- Description -->
            <div class="row">
            <div class="col-12">
              <div class="card border-light mb-3">
                <div class="card-header bg-primary text-white text-uppercase"><i class="fa fa-align-justify"></i> Description</div>
                <div class="card-body">
                    <p class="card-text">
                    ${leProduit.produit.description}
                    </p>                   
                </div>
            </div>
        </div>
            </div>
          </div> 
         <div class="row my-4">
          <div class="col-sm-12 text-center mb-3 col-md-6">
              <a class="btn btn-lg btn-block btn-secondary w-75 text-uppercase" role="button" href="index.html"> Accueil</a>
          </div>
          <div class="col-sm-12 col-md-6 text-center">
              <a class="btn btn-lg btn-block btn-success text-uppercase w-75" role="button" href="panier.html">Votre panier</a>
          </div>
          </div>`;

      main.innerHTML = mainContent;

      document.getElementById("addInCart").addEventListener("click", (e) => {

        e.preventDefault();
        let qte = document.getElementById("quantity");
        let color = document.getElementById("colors");

        if (color.options[color.selectedIndex].value == "") {
          document.getElementById("errorColor").classList.toggle("errors");
          document.getElementById("errorColor").innerHTML =
            "Veuillez choisir la couleur";
        } else {
          if (
            document.getElementById("errorColor").classList.contains("errors")
          ) {
            document.getElementById("errorColor").classList.toggle("errors");
          }

          document.getElementById("errorColor").innerHTML = "";
          qte = qte.value;
          color = color.options[color.selectedIndex].text;

          cartManager
            .ajouterAuPanier({
              id: leProduit.produit._id,
              name:leProduit.produit.name,
              urlImage: leProduit.produit.imageUrl,
              quantity: qte,
              color: color,
              price: leProduit.produit.price,
            })
            .then((quantity) => {
              document.getElementById("prix").innerHTML = quantity;
            });
           
            window.location.href = "panier.html";
        }
      });

    } else {
      // A Ecrire
    }
    
    const footer = new Footer();
    footer.render();

  });
} catch (error) {
  // A ecrire ...
  console.log(error);
}
