import Requete from "./modele/requete.js";

import Heading from "../components/heading/heading";
import Footer from "../components/footer/footer.js";

import "../../node_modules/bootstrap/dist/css/bootstrap.min.css";
import "../components/heading/heading.scss";

import "../components/Pages/confirmation.scss";

import "../../node_modules/jquery/dist/jquery.min";
import "../../node_modules/bootstrap/dist/js/bootstrap.bundle.min";

let mainContent = "";
const main = document.querySelector('main');
const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);
let id = urlParams.get("orderId");
let prix = urlParams.get("prix");
const requete = new Requete();
const url = "http://localhost:3000/api/teddies";

try {
    requete.get(url).
    then(liste =>{
        const head = new Heading();
        head.render(liste);

        mainContent  = `<div class="jumbotron text-center my-5">
       <div class="container">
         <h1 class="jumbotron-heading">Orinoco shop en ligne</h1>
         <p class="lead text-muted mb-0">
           Nous vous remercions pour votre commande d'un montant total de <strong> ${Math.round(prix).toFixed(2)} $</strong>, 
           et le numero de reference pour votre commande est <strong>${id}</strong>. 
         </p>
       </div>
     </div>`;

     main.innerHTML = mainContent;

     const footer = new Footer();
     footer.render();
    });
    
} catch (error) {
    console.log(error);
}