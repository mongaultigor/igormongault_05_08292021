
module.exports.checkInputs = () => {
    let valid = true;
    
    valid &= validateInputs("lastName", "Le nom est obligatoire");
    valid &= validateInputs("firstName", "Le prenom est obligatoire");
    valid &= validateInputs("address", "L'addresse est obligatoire");
    valid &= validateInputs("city", "La ville est obligatoire");
    valid &= validateInputs("email", "Le mail n'est pas valid");
  
    return {
      isValid: valid,
      firstName: document.getElementById("lastName").value.trim(),
      lastName: document.getElementById("lastName").value.trim(),
      address: document.getElementById("address").value.trim(),
      city: document.getElementById("city").value.trim(),
      email: document.getElementById("email").value.trim(),
    };
  
  }
  
  const validateInputs = (elt, message) => {
    
    let inputs = document.getElementById(elt);
    if (!inputs.checkValidity()) {
      setErrorFor(inputs, message);
    } else {
      setSuccessFor(inputs);
    }
  
    return inputs.checkValidity();
  };
  
  const setErrorFor = (input, message) => {
    const formControl = input.parentElement;
    const small = formControl.querySelector("small");
    formControl.classList.add("error");
    small.innerText = message;
  };
  
  const setSuccessFor = (input) => {
    const formControl = input.parentElement;
    if (formControl.classList.contains("error")) {
      formControl.classList.toggle("error");
    }
    formControl.classList.add("success");
  };