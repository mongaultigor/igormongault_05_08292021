const choixCouleur =  (liste) =>{
     let selects = "";
    for (let index = 0; index < liste.length; index++) {
        selects += `<option value="${index}">${liste[index]}</option>`
    }

    return  selects;
}

export default choixCouleur;
