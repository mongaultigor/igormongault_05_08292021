import * as cartManager from "../../js/modele/cartManager";
const validation = require('./validationForm');
import Requete from "../../js/modele/requete";

/**
 * Methode pour afficher le contenu du panier et
 * le formulaire de commande
 */

const displayCart = async () => {
  cartManager.recupererPanier().then((liste) => {
    const main = document.querySelector("main");
    const requete = new Requete();

    let product = [];
    let prixTot = 0;

    let mainContent = `
            <div class="jumbotron text-center">
            <div class="container my-4">
                <h1 class="jumbotron-heading">Commander</h1>
                <p class="lead text-muted mb-0"> Veuillez remplir le formulaire puis valider votre commande</p>
            </div>
           </div><div class="container mb-4">
            <div class="row">
                <div class="col-12">
                 <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col"> </th>
                            <th scope="col">Produits</th>
                            <th scope="col">couleur</th>
                            <th scope="col" class="text-center">Quantité</th>
                            <th scope="col" class="text-right">Prix ($)</th>
                            <th> </th>
                            <th> </th>
                        </tr>
                    </thead>
                    <tbody id="contentTabs">`;

    mainContent += tabsContent(liste);

    mainContent += `</tbody>
        </table>
        </div>
    </div>
    </div></div><div class="container">
    <div class="row">
        <div class="col">
            <form id="form" class="form" novalidate>
                <div class="form-group col-md-6 position-relative">
                     <label for="name">Nom </label>
                     <input type="text" class="form-control mt-1 border-2" id="firstName" aria-describedby="emailHelp" placeholder="Votre nom" required>
                     <i class="fas fa-check-circle position-absolute top-50 end-0 translate-middle"></i>
                     <i class="fas fa-exclamation-circle position-absolute top-50 end-0 translate-middle"></i>
                     <small>*******</small>
                 </div>
                 <div class="form-group col-md-6 position-relative">
                     <label for="name">Prenom </label>
                     <input type="text" class="form-control mt-1 border-2" id="lastName" aria-describedby="emailHelp" placeholder="Votre prenom" required>
                     <i class="fas fa-check-circle position-absolute top-50 end-0 translate-middle"></i>
                     <i class="fas fa-exclamation-circle position-absolute top-50 end-0 translate-middle"></i>
                     <small>Le prenom est obligatoire</small>
                 </div>
                 <div class="form-group col-md-6 position-relative">
                     <label for="name">Adresse</label>
                     <input type="text" class="form-control mt-1 border-2" id="address" aria-describedby="emailHelp" placeholder="Votre adresse" required>
                     <i class="fas fa-check-circle position-absolute top-50 end-0 translate-middle"></i>
                     <i class="fas fa-exclamation-circle position-absolute top-50 end-0 translate-middle"></i>
                     <small>L'adresse est obligatoire</small>
                 </div>
                 <div class="form-group col-md-6 position-relative">
                     <label for="name">Ville </label>
                     <input type="text" class="form-control mt-1 border-2" id="city" aria-describedby="emailHelp" placeholder="Votre ville" required>
                     <i class="fas fa-check-circle position-absolute top-50 end-0 translate-middle"></i>
                     <i class="fas fa-exclamation-circle position-absolute top-50 end-0 translate-middle"></i>
                     <small>La ville est obligatoire</small>
                 </div>
                 <div class="form-group col-md-6 position-relative">
                     <label for="email">Email</label>
                     <input type="email" class="form-control mt-1 border-2" id="email" aria-describedby="emailHelp" placeholder="votre email" required>
                     <i class="fas fa-check-circle position-absolute top-50 end-0 translate-middle"></i>
                     <i class="fas fa-exclamation-circle position-absolute top-50 end-0 translate-middle"></i>
                     <small>Le mail doit etre obligatoire</small>
                 </div>

                 <div my-5>
                     <div class="row">
                         <div class="col-sm-12  col-md-6 mt-4">
                             <a href="index.html" class="btn btn-lg btn-block btn-info">Continuer à magasiner</a>
                         </div>
                         <div class="col-sm-12 col-md-6 text-right mt-4">
                             <button type="submit" id="placeOrder" disabled class="btn btn-lg btn-block btn-success text-uppercase">Commander</button>
                         </div>
                     </div>
                 </div>
            </form>
        </div>
    </div>
   </div>`;

    main.innerHTML = mainContent;

    if (liste.length > 0) {
      document.getElementById("placeOrder").disabled = false;
    } else {
      document.getElementById("placeOrder").disabled = true;
    }

    /**
     * Ajout des evenements aux btn 
     * supprimer / modifier panier
     */

    const deleteItems = document.querySelectorAll(".deleteItems");

    deleteItems.forEach((element) => {
      element.addEventListener("click", async () => {

        const id = element.dataset.id;
        const colors = element.dataset.color;
       
        await cartManager.supprimerEltPanier(id,colors);

        const panier = await cartManager.recupererPanier();

        document.getElementById("prix").innerHTML =
          await cartManager.totalEltPanier(panier);

        let leTotal = await cartManager.amountCart(panier);

        document.getElementById("prixTot").innerHTML = `<strong>${Math.round(
          leTotal / 100
        ).toFixed(2)}</strong>`;

        product = panier.map((list) => list.id);

        if (panier.length > 0) {
          document.getElementById("placeOrder").disabled = false;
        } else {
          document.getElementById("placeOrder").disabled = true;
        }

        document.getElementById(`prod-${colors}${id}`).style.display = "none";
        
      });
    });

    /**
     * Ajout un evenement sur 
     * le bouton valider la commande
     */

    const form = document.getElementById("form");

    form.addEventListener("submit", async (e) => {

      e.preventDefault();

      const panier = await cartManager.recupererPanier();

      product = panier.map((list) => list.id);

      prixTot =  await cartManager.amountCart(panier);
     
      if (validation.checkInputs().isValid) {
        const order = {
          contact: {
            firstName: validation.checkInputs().firstName,
            lastName: validation.checkInputs().lastName,
            address: validation.checkInputs().address,
            city: validation.checkInputs().city,
            email: validation.checkInputs().email,
          },
          products: product,
        };
        const getOrder = await requete.post("http://localhost:3000/api/teddies/order", order);

        await cartManager.supprimerTout();

        window.location.href = `confirmation.html?orderId=${
          getOrder.orderId
        }&prix=${Math.round(prixTot / 100).toFixed(2)}`;

      }  
     
    });

  });

};


const tabsContent = (listItems) => {
  let tabs = "";
  let prixTotal = 0;

  for (let index = 0; index < listItems.length; index++) {
    tabs += `<tr id="prod-${listItems[index].color}${listItems[index].id}">
                      <td><img src="${listItems[index].urlImage}"> </td>
                      <td>${listItems[index].name}</td>
                      <td>${listItems[index].color}</td>
                      <td class="text-center">${listItems[index].quantity}</td>
                      <td class="text-right">${Math.round(
                        (listItems[index].price * listItems[index].quantity) /
                          100
                      ).toFixed(2)}</td>
                      <td class="text-right">
                      <button class="btn btn-sm btn-danger deleteItems" data-id="${
                        listItems[index].id
                      }" data-color="${listItems[index].color}"><i class="fa fa-trash"></i> </button> 
                      </td> 
              </tr>`;

    prixTotal += listItems[index].quantity * parseInt(listItems[index].price);
  }

  tabs += `<tr>
          <td></td>
            <td></td>
            <td></td>
            <td><strong>Total</strong></td>
            <td id="prixTot" class="text-right"><strong>${Math.round(
              prixTotal / 100
            ).toFixed(2)} </strong></td>
          </tr>`;

  return tabs;
};

export default displayCart;
