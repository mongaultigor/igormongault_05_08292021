const path = require('path');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');


module.exports = {

    mode: "none",
    entry: {
       'index' :'./src/js/index.js',
       'produits': './src/js/produits.js',
       'panier': './src/js/panier.js',
       'confirmation': './src/js/confirmation.js'
    },
    output: {
      filename: '[name].bundle.js',
      path: path.resolve(__dirname,'dist')
    },
    devServer: {
      open: true,
      contentBase: path.resolve(__dirname,'./dist'),
      index: 'index.html',
      port:9000,
      writeToDisk: true
    },

    module:{
        rules: [
            {
                test: /\.(png|jpg)$/i,
                type:'asset',
                parser:{
                    dataUrlCondition: {
                         maxSize: 3 * 1024 // 3 kilobytes
                    }
                }  
            },
            {
                test: /\.txt/,
                type: 'asset/source'
            },
            {
                test: /\.(scss|css)$/,
             use: [
                MiniCssExtractPlugin.loader, 'css-loader',
                'postcss-loader'
                , 'sass-loader'
             ]
         },
         {
             test: /\.js$/,
             exclude: /node_modules/,
             use:{
                 loader: 'babel-loader',
                 options:{
                     presets: ['@babel/preset-env'],
                     plugins: ['@babel/plugin-proposal-class-properties'],
                     plugins: ["@babel/plugin-transform-runtime"]
                 }
             }
         },
         {
            test: /\.json$/,
            loader: 'json-loader'
          } 
         ,{
             test: /\.hbs$/,
             use:[
                 'handlebars-loader'
             ]
         } 
        ]
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: "[name].style.css"
        }),
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin(
            {
           filename: 'index.html',
           chunks: ['index'],
           title: 'Orinoco Ours en Peluche',
           template: 'src/page-template.hbs',
           inject: 'body',
           description: "Onrinoco est un site de vente d'ours en peluche fait à la main",
           minify: false  
        }),new HtmlWebpackPlugin({
            filename: 'produits.html',
            chunks: ['produits'],
            title: 'details sur des ours',
            template: 'src/page-template.hbs',
            inject: 'body',
            description: 'Informations detaillees des Ours en peluche et personnalisation',
            minify: false
            
         }),new HtmlWebpackPlugin({
            filename: 'panier.html',
            chunks: ['panier'],
            title: 'Details du panier',
            template: 'src/page-template.hbs',
            inject: 'body',
            description: 'Informations detaillees du panier contenant les ours en peluche du client',
            minify: false
            
         }),new HtmlWebpackPlugin({
            filename: 'confirmation.html',
            chunks: ['confirmation'],
            title: 'Confirmation de la commande',
            template: 'src/page-template.hbs',
            inject: 'body',
            description: 'Page de la confirmation de la commande des ours en peluche',
            minify: false
            
         })
    ]
    

}